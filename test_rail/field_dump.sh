#!/bin/bash -
#===============================================================================
#
#          FILE: field_dump.sh
# 
#         USAGE: ./field_dump.sh 
# 
#   DESCRIPTION: Pulls custom fields out of a test rail project ID.
# 
#       OPTIONS: $1 - Test Rail ID
#  REQUIREMENTS: ---
#          BUGS: ---
#         NOTES: ---
#        AUTHOR: Chris Adler, 
#  ORGANIZATION: Byton 
#       CREATED: 04/10/2019 14:30
#      REVISION:  ---
#===============================================================================

set -o nounset                              # Treat unset variables as an error

[[ $# -eq 0 ]] && echo "You must provide a Test Rail project ID." && exit

json_data=$(curl -s -H "Content-Type: application/json" \
	-u "chris.adler@byton.com:touchme" \
	https://locomotive.lab.byton.io/index.php?/api/v2/get_case_fields)\
	2>&1 || exit 1

jq --raw-output '.[] | select(.configs[].context.project_ids != null) | select (.configs[].context.project_ids[] == '$1') | .label' <<< $json_data | uniq

